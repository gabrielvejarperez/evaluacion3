/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.appeva3.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.appeva3.dao.CandidatoJpaController;
import root.appeva3.dao.exceptions.NonexistentEntityException;
import root.appeva3.entity.Candidato;

/**
 *
 * @author gveja
 */
@Path("candidato")
public class CandidatoRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaCandidatos() {
        CandidatoJpaController dao = new CandidatoJpaController();
        List<Candidato> lista = dao.findCandidatoEntities();
        return Response.ok(200).entity(lista).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Candidato candidato) {
        CandidatoJpaController dao = new CandidatoJpaController();
        try {
            dao.create(candidato);
        } catch (Exception ex) {
            Logger.getLogger(CandidatoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(candidato).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Candidato candidato) {
        CandidatoJpaController dao = new CandidatoJpaController();
        try {
            dao.edit(candidato);
        } catch (Exception ex) {
            Logger.getLogger(CandidatoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(candidato).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")
    public Response eliminar(@PathParam("rut") String rut) {
        CandidatoJpaController dao = new CandidatoJpaController();
        try {
            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CandidatoRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Candidato eliminado").build();
    }

}
